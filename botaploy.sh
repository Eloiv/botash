#!/bin/bash
clear

if [[ $1 = 'dev' ]]; then
  modules_enable=(search_kint, kint, devel_generate, devel, views_ui, field_ui, update, help, simpletest, image_styles_mapping, advanced_help, block_visibility_groups_admin, config_update, flood_unblock)
  css_preprocess=0;
  js_preprocess=0;
elif [[ $1 = 'pre' ]]; then
  modules_disable=(search_kint, kint, devel_generate, devel, views_ui, field_ui, update, help, simpletest, image_styles_mapping, advanced_help, block_visibility_groups_admin, config_update, flood_unblock)
  css_preprocess=1;
  js_preprocess=1;
elif [[ $1 = 'pro' ]]; then
  modules_disable=(search_kint, kint, devel_generate, devel, views_ui, field_ui, update, help, simpletest, image_styles_mapping, advanced_help, block_visibility_groups_admin, config_update, flood_unblock)
  css_preprocess=1;
  js_preprocess=1;
else
  echo "You need to pass the Enviroment (dev,pre,pro) through param"
  echo "-------------------------"
  echo "Example 1: botaploy.sh dev"
  echo "Example 2: botaploy.sh pre"
  echo "Example 3: botaploy.sh pro"
fi


if ! [ -z "$1" ]
  then
  tput setaf 40; echo "[ENVIROMENT $1] start deploy on $1"; tput sgr0
  tput setaf 40; echo "[GIT] pull master branch"; tput sgr0
  git pull origin master
  tput setaf 40; echo "[CONFIG] drush cim"; tput sgr0
  drush cim -y
  drush cr

  tput setaf 40; echo "[PERFORMANCE] enable js and css"; tput sgr0
  drush config-set system.performance css.preprocess $css_preprocess -y
  drush config-set system.performance js.preprocess $js_preprocess -y


  tput setaf 40; echo "[MODULES] disable next modules"; tput sgr0
  for i in ${modules_disable[@]}; do
      drush pmu $i -y
      drush cr
  done
  tput setaf 40; echo "[MODULES] enable next modules"; tput sgr0
  for i in ${modules_enable[@]}; do
      drush en $i -y
      drush cr
  done

  tput setaf 40; echo "[CRON] execute cron"; tput sgr0
  drush cron
  drush cr
fi
